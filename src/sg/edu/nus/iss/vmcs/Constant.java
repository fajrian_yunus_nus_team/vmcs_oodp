package sg.edu.nus.iss.vmcs;

public class Constant {
	public static final int COIN_INPUT_LISTENER = 1;
	public static final int DRINK_SELECTION_LISTENER = 2;
	public static final int TERMINATE_BUTTON_LISTENER = 3;

}
