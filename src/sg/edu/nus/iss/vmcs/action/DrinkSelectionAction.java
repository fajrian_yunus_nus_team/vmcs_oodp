package sg.edu.nus.iss.vmcs.action;

import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import sg.edu.nus.iss.vmcs.customer.DrinkSelectionItem;

public class DrinkSelectionAction extends VMCSAction{

	@Override
	public void processActionPerformed(ActionEvent ev) {
		Object obj=ev.getSource();
		Button btn=(Button)obj;
		btn.requestFocus();
		mainController.getTransactionController().startTransaction(((DrinkSelectionItem)btn.getParent()).getDrinkIdentifier());
		btn.setBackground(Color.yellow);
	}

}
