package sg.edu.nus.iss.vmcs.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class VMCSActionListener implements ActionListener{

	private VMCSAction vMCSAction;	
	
	public VMCSActionListener(VMCSAction vMCSAction) {
		this.vMCSAction = vMCSAction;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		vMCSAction.processActionPerformed(e);
	}

}
