package sg.edu.nus.iss.vmcs.action;

import java.awt.event.ActionEvent;

public class TerminateButtonAction extends VMCSAction{

	@Override
	public void processActionPerformed(ActionEvent ev) {
		mainController.getTransactionController().cancelTransaction();
	}

}
