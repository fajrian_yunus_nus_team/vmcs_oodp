package sg.edu.nus.iss.vmcs.action;

import java.awt.event.ActionListener;

import sg.edu.nus.iss.vmcs.Constant;
import sg.edu.nus.iss.vmcs.customer.TransactionController;

public class ActionListenerFactory {
	
	public static ActionListener createActionListener(int type,TransactionController trnxCtrl){
		VMCSActionListener actionListener = null;
		if(type == Constant.COIN_INPUT_LISTENER) {			
			actionListener = new VMCSActionListener(new CoinInputAction(trnxCtrl));
		}else if(type == Constant.DRINK_SELECTION_LISTENER) {			
				actionListener = new VMCSActionListener(new DrinkSelectionAction());
		}else if(type == Constant.TERMINATE_BUTTON_LISTENER) {			
			actionListener = new VMCSActionListener(new TerminateButtonAction());				
		}else {
			System.out.println("invalid type or the type hasn't been implemented");
		}
		
		return actionListener;
	}
}
