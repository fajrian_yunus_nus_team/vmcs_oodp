package sg.edu.nus.iss.vmcs.action;

import java.awt.event.ActionEvent;

import sg.edu.nus.iss.vmcs.Vmcs;
import sg.edu.nus.iss.vmcs.system.MainController;

public abstract class VMCSAction {
	
	protected MainController mainController;
	
	public VMCSAction() {
		this.mainController = Vmcs.getResource();
	}
	
	public abstract void processActionPerformed(ActionEvent ev);
}
