package sg.edu.nus.iss.vmcs.action;

import java.awt.event.ActionEvent;

import sg.edu.nus.iss.vmcs.customer.CoinButton;
import sg.edu.nus.iss.vmcs.customer.CoinReceiver;
import sg.edu.nus.iss.vmcs.customer.PaymentController;
import sg.edu.nus.iss.vmcs.customer.TransactionController;

public class CoinInputAction extends VMCSAction{

    
    CoinReceiver coinReceiver;
	
	public CoinInputAction(TransactionController txCtrl){
            
             txCtrl.setPaymentMethod(PaymentController.CASH);
            this.coinReceiver=txCtrl.getPaymentController().getCoinReceiver();
                
	}
	@Override
	public void processActionPerformed(ActionEvent ev) {
		// TODO Auto-generated method stub
		CoinButton coinButton=(CoinButton)ev.getSource();
		System.out.println("coinButton:"+coinButton.getWeight());
		mainController.getCashController().getCoinReceiver().receiveCoin(coinButton.getWeight());
	}

}
