package sg.edu.nus.iss.vmcs.customer;

import java.util.HashMap;
import java.util.Map;

import sg.edu.nus.iss.vmcs.maintenance.MaintenanceController;
import sg.edu.nus.iss.vmcs.store.StoreItem;

public class DrinkSelectionItemsStateMediator {
	private Map<DrinkSelectionItem, StoreItem> drinkSelectionItemToStoreItemMap;

	private TransactionController transactionController;
	private MaintenanceController maintenanceController;
	
	public DrinkSelectionItemsStateMediator(
			TransactionController _transactionController,
			MaintenanceController _maintenanceController,
			Map<DrinkSelectionItem, StoreItem> _drinkSelectionItemToStoreItemMap) {
		this.transactionController = _transactionController;
		this.maintenanceController = _maintenanceController;
		this.drinkSelectionItemToStoreItemMap = new HashMap<DrinkSelectionItem, StoreItem>();
		this.drinkSelectionItemToStoreItemMap.putAll(_drinkSelectionItemToStoreItemMap);
		this.updateState();
	}
	
	public void updateState() {
		for (DrinkSelectionItem key : this.drinkSelectionItemToStoreItemMap.keySet()) {
			this.updateState(key);
		}
	}

	private void updateState(DrinkSelectionItem drinkSelectionItem) {
		if (!this.drinkSelectionItemToStoreItemMap.containsKey(drinkSelectionItem)) {
			return;
		}
		
		boolean isTransactionRunning = this.isTransactionRunning();
		boolean hasStock = this.hasStock(this.drinkSelectionItemToStoreItemMap.get(drinkSelectionItem));
		boolean isMaintenanceActive = this.isMaintenancePanelActive();
		
		boolean buttonActive =
				!isTransactionRunning &&
				hasStock &&
				!isMaintenanceActive;
		boolean outOfStockWarningActive = !hasStock;

		drinkSelectionItem.setOutOfStockWarningState(outOfStockWarningActive);
		drinkSelectionItem.setButtonState(buttonActive);
	}
	
	private boolean isTransactionRunning() {
		return this.transactionController.isTransactionRunning();
	}
	
	private boolean hasStock(StoreItem storeItem) {
		return storeItem.getQuantity() > 0;
	}
	
	private boolean isMaintenancePanelActive() {
		return this.maintenanceController.isMaintainerActive();
	}
}
