package sg.edu.nus.iss.vmcs.customer;
import sg.edu.nus.iss.vmcs.payment.api.CreditCardAPI;

public class CreditCardController extends PaymentController {

    private CreditCardReceiver receiver;
    public CreditCardController(TransactionController trnxController) {
        super(trnxController);
        receiver=new CreditCardReceiver(this);
    }

    @Override
    public boolean makePayment() {
        
        dispenseCtrl.ResetCan();
        trnxController.getCustomerPanel().setCoinInputBoxActive(true);
        trnxController.getCustomerPanel().setTotalMoneyInserted(0);
         receiver.startReceiver();
         return true;
    }

     public void processCardReceived(String cardNumber){
         CreditCardAPI ccAPI=new CreditCardAPI();
          boolean isTransactionSuccess=ccAPI.processTransaction(cardNumber,trnxController.getPrice());
          if(isTransactionSuccess)processMoneyReceived(trnxController.getPrice());
    }
    
    @Override
    public void processMoneyReceived(int total) {completeTransaction();}

    @Override
    public void completeTransaction() {
        System.out.println("Payment have been completed by Credit Card");
        trnxController.getCustomerPanel().setCreditCardPaymentActive(false);
        trnxController.getCustomerPanel().setNetPaymentActive(false);
        trnxController.getCustomerPanel().setCoinInputBoxActive(false);
        dispenseCtrl.dispenseDrink(trnxController.getSelection());
        
    }
    
    
    public String getCardNumber() {
        return "12345676687";
    }


   

    @Override
    public CoinReceiver getCoinReceiver() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NETSCardReceiver getNETSCardReceiver() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CreditCardReceiver getCreidtCardReceiver() throws UnsupportedOperationException {
       return receiver;
    }
}
