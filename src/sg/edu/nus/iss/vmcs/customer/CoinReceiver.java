/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

import java.util.ArrayList;

import sg.edu.nus.iss.vmcs.machinery.MachineryController;
import sg.edu.nus.iss.vmcs.store.CashStore;
import sg.edu.nus.iss.vmcs.store.Coin;
import sg.edu.nus.iss.vmcs.store.Store;
import sg.edu.nus.iss.vmcs.util.VMCSException;

public class CoinReceiver{
	private PaymentController paymentController;
	public CoinReceiver(PaymentController paymentController){
		this.paymentController=paymentController;
		arlCoins=new ArrayList();
		totalInserted=0;
	}
	
	public void startReceiver(){
		paymentController.getCustomerPanel().setCoinInputBoxActive(true);
		paymentController.getCustomerPanel().setTotalMoneyInserted(0);
             
	}
	
	public void receiveCoin(double weight){
		CashStore cashStore=(CashStore)paymentController.getTrnxController().getMainController().getStoreController().getStore(Store.CASH);
		Coin coin=cashStore.findCoin(weight);
		if(coin==null){
			paymentController.getCustomerPanel().displayInvalidCoin(true);
			paymentController.getCustomerPanel().setChange("Invalid Coin");
		}
		else{
                    
                       paymentController.getCustomerPanel().setNetPaymentActive(false);
                        paymentController.getCustomerPanel().setCreditCardPaymentActive(false);
			paymentController.getCustomerPanel().setCoinInputBoxActive(false);
			int value=coin.getValue();
			paymentController.getCustomerPanel().displayInvalidCoin(false);
			arlCoins.add(coin);
			setTotalInserted(getTotalInserted() + value);
			paymentController.getCustomerPanel().setTotalMoneyInserted(getTotalInserted());
			paymentController.getCustomerPanel().setChange("");
                        paymentController.processMoneyReceived(getTotalInserted());
		}
	}

	private ArrayList arlCoins;
	private int totalInserted;
	public void continueReceive(){
		paymentController.getCustomerPanel().setCoinInputBoxActive(true);
	}
	
	/**
	 * Instruct the Cash Store to update its totals and then re-set the Total
	 * Money Inserted Display to zero.
	 * @return return TRUE if cash has been stored, else return FALSE.
	 */
	public boolean storeCash(){
		MachineryController machineryCtrl=paymentController.getTrnxController().getMainController().getMachineryController();
		try{
			for(int i=0;i<arlCoins.size();i++){
				Coin coin=(Coin)arlCoins.get(i);
				machineryCtrl.storeCoin(coin);
			}
			resetReceived();
			paymentController.getCustomerPanel().setTotalMoneyInserted(0);
		}
		catch(VMCSException ex){
			paymentController.terminateFault();
			return false;
		}
		return true;
	}
	
	/**
	 * This method will deactivate the Coin Input Box in order to stop 
	 * receiving coins.
	 */
	public void stopReceive(){
		CustomerPanel custPanel=paymentController.getCustomerPanel();
		if(custPanel==null){
			return;
		}
		custPanel.setCoinInputBoxActive(false);
	}
	
	/**
	 * This method handles the refunding of Coins entered so far to 
	 * the Customer.
	 */
	public void refundCash(){
		if(getTotalInserted()==0)
			return;
		paymentController.getCustomerPanel().setChange(getTotalInserted());
		paymentController.getCustomerPanel().setTotalMoneyInserted(0);
		paymentController.getCustomerPanel().displayInvalidCoin(false);
		resetReceived();
	}
	
	/**
	 * This method reset the coin received input.
	 */
	public void resetReceived(){
		arlCoins=new ArrayList();
		setTotalInserted(0);
	}
	
	/**
	 * This method activates or deactivates the Coin Input Box.
	 * @param active TRUE to activate, FALSE to deactivate the Coin Input Box.
	 */
	public void setActive(boolean active){
		paymentController.getCustomerPanel().setCoinInputBoxActive(active); 
	}

	/**
	 * This method sets the total money inserted.
	 * @param totalInserted the total money inserted.
	 */
	public void setTotalInserted(int totalInserted) {
		this.totalInserted = totalInserted;
	}

	/**
	 * This method returns the total money inserted.
	 * @return the total money inserted.
	 */
	public int getTotalInserted() {
		return totalInserted;
	}
}//End of class CoinReceiver