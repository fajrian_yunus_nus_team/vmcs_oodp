/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.vmcs.customer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NETSCardReaderListener implements ActionListener{
        NETSCardReceiver cardReceiver;
	
	
	public NETSCardReaderListener(TransactionController txCtrl){
             txCtrl.setPaymentMethod(PaymentController.NETS);
            this.cardReceiver=txCtrl.getPaymentController().getNETSCardReceiver();
                
	}
	
	public void actionPerformed(ActionEvent ev){
          
		cardReceiver.onReceiveCard();
	}
}
