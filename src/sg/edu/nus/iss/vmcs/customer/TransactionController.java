/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
import java.awt.Frame;

import sg.edu.nus.iss.vmcs.store.DrinksBrand;
import sg.edu.nus.iss.vmcs.store.Store;
import sg.edu.nus.iss.vmcs.store.StoreItem;
import sg.edu.nus.iss.vmcs.system.MainController;
import sg.edu.nus.iss.vmcs.system.SimulatorControlPanel;

/**
 * This control object coordinates the customer transactions for selection of a
 * drink brand, coin input, storage of coins and termination requests for
 * ongoing transactions.
 *
 * @author Team SE16T5E
 * @version 1.0 2008-10-01
 */
public class TransactionController implements DrinkSelectionItemBigMouthedColleague {
    
    private CustomerPanel custPanel;
    private PaymentController paymentController;
    private boolean transactionRunning;
    
    public void setPaymentMethod(int type) {
        paymentController = PaymentFactory.createPayment(this, type);
    }

    public void startTransaction(int drinkIdentifier) {
        this.transactionRunning = true;
        this.updateDrinkSelectionItemsState();
        this.getCustomerPanel().setNetPaymentActive(true);
        this.getCustomerPanel().setCreditCardPaymentActive(true);
        selection = drinkIdentifier;
        StoreItem storeItem = mainCtrl.getStoreController().getStoreItem(Store.DRINK, drinkIdentifier);
        storeItem.addObserver(paymentController.getDispenseCtrl());
        storeItem.addObserver(mainCtrl.getMachineryController());
        storeItem.addObserver(mainCtrl.getMaintenanceController());
        DrinksBrand drinksBrand = (DrinksBrand) storeItem.getContent();
        price = drinksBrand.getPrice();
        paymentController.makePayment();
        custPanel.setTerminateButtonActive(true);
    }
    
  
    public void terminateFault() {
        System.out.println("TerminateFault: Begin");
        paymentController.terminateFault();
        refreshMachineryDisplay();
        System.out.println("TerminateFault: End");
    }

    
    public void terminateTransaction() {
        System.out.println("TerminateTransaction: Begin");
        paymentController.terminateTransaction();
        if (custPanel != null) {
            custPanel.setTerminateButtonActive(false);
        }
        refreshMachineryDisplay();
        this.transactionRunning = false;
        this.updateDrinkSelectionItemsState();
        System.out.println("TerminateTransaction: End");
    }

    
    public void cancelTransaction() {
        System.out.println("CancelTransaction: Begin");
        paymentController.cancelTransaction();
        refreshMachineryDisplay();
        this.transactionRunning = false;
        this.updateDrinkSelectionItemsState();
        System.out.println("CancelTransaction: End");
    }
    
    private MainController mainCtrl;
    private boolean changeGiven = false;
    private boolean drinkDispensed = false;
    private int price = 0;
    private int selection = -1;

    
    public TransactionController(MainController mainCtrl) {
        this.mainCtrl = mainCtrl;
        this.transactionRunning = false;
    }

    
    public MainController getMainController() {
        return mainCtrl;
    }

    /**
     * This method displays and initialize the CustomerPanel.
     */
    public void displayCustomerPanel() {
        SimulatorControlPanel scp = mainCtrl.getSimulatorControlPanel();
        custPanel = new CustomerPanel((Frame) scp, this);
        custPanel.display();
        paymentController.displayCustomerPanel();
    }

    
    

    /**
     * This method refreshes the CustomerPanel when maintainer logs-out.
     */
    public void refreshCustomerPanel() {
       
        paymentController.refreshCustomerPanel();
        custPanel.setTerminateButtonActive(true);
    }

    /**
     * This method will close down the transaction control function of the
     * vending machine.
     */
    public void closeDown() {
        if (custPanel != null) {
            custPanel.closeDown();
        }
    }

    /**
     * This method sets whether the change is given.
     *
     * @param changeGiven TRUE the change is given, otherwise FALSE.
     */
    public void setChangeGiven(boolean changeGiven) {
        this.changeGiven = changeGiven;
    }

    /**
     * This method returns whether the change is given.
     *
     * @return TRUE if the change is given, otherwise FALSE.
     */
    public boolean isChangeGiven() {
        return changeGiven;
    }

    /**
     * This method sets whether the drink is dispensed.
     *
     * @param drinkDispensed TRUE the drink is dispensed, otherwise, FALSE.
     */
    public void setDrinkDispensed(boolean drinkDispensed) {
        this.drinkDispensed = drinkDispensed;
    }

    /**
     * This method returns whether the drink is dispensed.
     *
     * @return TRUE if the drink is dispensed, otherwise FALSE.
     */
    public boolean isDrinkDispensed() {
        return drinkDispensed;
    }

    /**
     * This method returns the CustomerPanel.
     *
     * @return the CustomerPanel.
     */
    public CustomerPanel getCustomerPanel() {
        return custPanel;
    }

    /**
     * This method refreshes the MachinerySimulatorPanel.
     */
    public void refreshMachineryDisplay() {
        mainCtrl.getMachineryController().refreshMachineryDisplay();

    }

    /**
     * This method will nullify reference to customer panel.
     */
    public void nullifyCustomerPanel() {
        custPanel = null;
    }

    public PaymentController getPaymentController() {
        return paymentController;
    }

    public void setPaymentController(PaymentController paymentController) {
        this.paymentController = paymentController;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSelection() {
        return selection;
    }

    public void setSelection(int selection) {
        this.selection = selection;
    }
    
   public boolean isTransactionRunning() {
           return this.transactionRunning;
   }
   
   public void setTransactionRunning(boolean _transactionRunning) {
           this.transactionRunning = _transactionRunning;
   }

   @Override
   public void updateDrinkSelectionItemsState() {
           this.custPanel.getDrinkSelectionBox().getDrinkSelectionItemsStateMediator().updateState();
   }

}//End of class TransactionController
