/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.vmcs.customer;


public class PaymentFactory {
    
    public static PaymentController createPayment(TransactionController trnxController,int paymentType){
     switch(paymentType){
         case PaymentController.CASH: return new CashController(trnxController);
         case PaymentController.CREDIT_CARD: return new CreditCardController(trnxController);
         case PaymentController.NETS: return new NETSController(trnxController);
         default : return new CashController(trnxController);
     }
    }
    
   
    
}
