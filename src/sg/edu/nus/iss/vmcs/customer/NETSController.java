package sg.edu.nus.iss.vmcs.customer;

import sg.edu.nus.iss.vmcs.payment.api.NETSAPI;

public class NETSController extends PaymentController {
    private NETSCardReceiver receiver;
    
    public NETSController(TransactionController trnxController) {
        super(trnxController);
        receiver=new NETSCardReceiver(this);
    }
    
    @Override
    public boolean makePayment() {
        
        dispenseCtrl.ResetCan();
        trnxController.getCustomerPanel().setCoinInputBoxActive(true);
        trnxController.getCustomerPanel().setTotalMoneyInserted(0);
        receiver.startReceiver();
        return true;
    }
    
    public void processCardReceived(String cardNumber){
        NETSAPI netsapi=new NETSAPI();
          boolean isTransactionSuccess=netsapi.makeTransaction(cardNumber, trnxController.getPrice());
          if(isTransactionSuccess)processMoneyReceived(trnxController.getPrice());
    }
    
    @Override
    public void processMoneyReceived(int total) {completeTransaction();}
    
    @Override
    public void completeTransaction() {
        System.out.println("Payment have been completed by NETS");
        trnxController.getCustomerPanel().setCreditCardPaymentActive(false);
        trnxController.getCustomerPanel().setNetPaymentActive(false);
        trnxController.getCustomerPanel().setCoinInputBoxActive(false);
        dispenseCtrl.dispenseDrink(trnxController.getSelection());
       
        super.completeTransaction();
    }
    

    


    public NETSCardReceiver getCardReceiver()  {
       return receiver;
    }

    @Override
    public CoinReceiver getCoinReceiver() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NETSCardReceiver getNETSCardReceiver() throws UnsupportedOperationException {
       return receiver;
    }

    @Override
    public CreditCardReceiver getCreidtCardReceiver() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
