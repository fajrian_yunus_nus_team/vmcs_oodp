/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class CoinInputListener implements ActionListener{
	CoinReceiver coinReceiver;
	
	public CoinInputListener(TransactionController txCtrl){
            
             txCtrl.setPaymentMethod(PaymentController.CASH);
            this.coinReceiver=txCtrl.getPaymentController().getCoinReceiver();
                
	}
	
	
	public void actionPerformed(ActionEvent ev){
          
		CoinButton coinButton=(CoinButton)ev.getSource();
		coinReceiver.receiveCoin(coinButton.getWeight());
	}
}


//End of class CoinInputListener