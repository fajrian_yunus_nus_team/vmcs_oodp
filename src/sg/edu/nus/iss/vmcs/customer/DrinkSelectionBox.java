/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.util.HashMap;
import java.util.Map;

import sg.edu.nus.iss.vmcs.Constant;
import sg.edu.nus.iss.vmcs.action.ActionListenerFactory;
import sg.edu.nus.iss.vmcs.store.DrinksBrand;
import sg.edu.nus.iss.vmcs.store.DrinksStoreItem;
import sg.edu.nus.iss.vmcs.store.Store;
import sg.edu.nus.iss.vmcs.store.StoreController;
import sg.edu.nus.iss.vmcs.store.StoreItem;
import sg.edu.nus.iss.vmcs.store.StoreObject;
import sg.edu.nus.iss.vmcs.system.MainController;

/**
 * This interface object is part of the Customer Panel&#46; It is used by the Customer to select a drink.
 * @author Team SE16T5E
 * @version 1.0 2008-10-01
 */
public class DrinkSelectionBox extends Panel{
	private DrinkSelectionItem drinkSelectionItems[];
	private TransactionController txCtrl;
	private DrinkSelectionItemsStateMediator drinkSelectionItemsStateMediator;
	
	/**Array of integers providing identifiers for each selection button.*/
	
	/**
	 * This constructor creates an instance of the object.
	 * @param txCtrl the Transaction Controller
	 */
	public DrinkSelectionBox(TransactionController txCtrl){
		this.txCtrl=txCtrl;
		MainController mainCtrl=txCtrl.getMainController();
		StoreController storeCtrl=mainCtrl.getStoreController();
		int drinkStoreSize=storeCtrl.getStoreSize(Store.DRINK);
		StoreItem[] drinkStoreItems=storeCtrl.getStore(Store.DRINK).getItems();
		
		drinkSelectionItems=new DrinkSelectionItem[drinkStoreSize];
		Map<DrinkSelectionItem, StoreItem> drinkSelectionItemToStoreItemMap = new HashMap<DrinkSelectionItem, StoreItem>();
		
		setLayout(new GridBagLayout());
		for(int i=0;i<drinkStoreItems.length;i++){
			StoreItem storeItem=drinkStoreItems[i];
			DrinksStoreItem drinksStoreItem=(DrinksStoreItem)storeItem;
			StoreObject storeObject=drinksStoreItem.getContent();
			DrinksBrand drinksBrand=(DrinksBrand)storeObject;
			String drinksName=drinksBrand.getName();
			int drinksPrice=drinksBrand.getPrice();
			int drinksQuantity=drinksStoreItem.getQuantity();
			drinkSelectionItems[i]=new DrinkSelectionItem(i,drinksName,drinksPrice,drinksQuantity,true,false);
//			drinkSelectionItems[i].addListener(new DrinkSelectionListener(txCtrl,i));
			drinkSelectionItems[i].addListener(ActionListenerFactory.createActionListener(Constant.DRINK_SELECTION_LISTENER,txCtrl));

			add(drinkSelectionItems[i],new GridBagConstraints(0,i,1,1,1.0,0.0,
				    GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,
				    new Insets(5,0,0,0),10,0));  
			drinkSelectionItemToStoreItemMap.put(drinkSelectionItems[i], storeItem);
		}
		drinkSelectionItemsStateMediator = new DrinkSelectionItemsStateMediator(
				this.txCtrl,
				mainCtrl.getMaintenanceController(),
				drinkSelectionItemToStoreItemMap);
	}
	
	public DrinkSelectionItemsStateMediator getDrinkSelectionItemsStateMediator() {
		return this.drinkSelectionItemsStateMediator;
	}
	
	/**
	 * This method updates the stock status, name and price of a drink brand based on the values received.
	 * @param brand the brand of the drink
	 * @param price the price of the drink
	 * @param name the name of the 
	 */
	public void update(int brand, int price, String name){
		if(drinkSelectionItems==null||drinkSelectionItems.length==0){
			return;
		}
		DrinkSelectionItem item=drinkSelectionItems[brand];
		item.setPrice(price);
		item.setName(name);
	}
	
	
}//DrinkSelectionBox