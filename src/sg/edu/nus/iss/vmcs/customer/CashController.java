
package sg.edu.nus.iss.vmcs.customer;

public class CashController extends PaymentController {
    private ChangeGiver changeGiver;
    private CoinReceiver coinReceiver;
    public CashController(TransactionController trnxController) {
        super(trnxController);
        coinReceiver = new CoinReceiver(this);
        changeGiver = new ChangeGiver(trnxController);

    }
    @Override
    public boolean makePayment() {
        changeGiver.resetChange();
        dispenseCtrl.ResetCan();
        changeGiver.displayChangeStatus();
        coinReceiver.startReceiver();
        return true;
    }

    @Override
    public void processMoneyReceived(int total) {
        if (total >= trnxController.getPrice()) {
            completeTransaction();
        } else {
            coinReceiver.continueReceive();
        }
    }

    @Override
    public void completeTransaction() {
        dispenseCtrl.dispenseDrink(trnxController.getSelection());
        int totalMoneyInserted = coinReceiver.getTotalInserted();
        int change = totalMoneyInserted - trnxController.getPrice();
        if (change > 0) {
            changeGiver.giveChange(change);
        } else {
            getCustomerPanel().setChange(0);
        }
        coinReceiver.storeCash();
       
        super.completeTransaction();
    }
    
    
     @Override
    public void displayCustomerPanel() {
        super.displayCustomerPanel();
        coinReceiver.setActive(false);
        changeGiver.displayChangeStatus();
    }

    public void terminateFault() {

        super.terminateFault();
        coinReceiver.refundCash();

    }

    public void terminateTransaction() {

        super.terminateTransaction();
        coinReceiver.stopReceive();
        coinReceiver.refundCash();

    }

    public void cancelTransaction() {

        coinReceiver.stopReceive();
        coinReceiver.refundCash();
        super.cancelTransaction();
    }

    public void refreshCustomerPanel() {

        super.refreshCustomerPanel();
        changeGiver.displayChangeStatus();

    }

    public ChangeGiver getChangeGiver() {
        return changeGiver;
    }

    public void setChangeGiver(ChangeGiver changeGiver) {
        this.changeGiver = changeGiver;
    }

    @Override
    public CoinReceiver getCoinReceiver() {
        return coinReceiver;
    }

   

    @Override
    public NETSCardReceiver getNETSCardReceiver() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CreditCardReceiver getCreidtCardReceiver() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
   

}
