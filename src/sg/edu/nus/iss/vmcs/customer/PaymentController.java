/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.vmcs.customer;

public abstract class PaymentController {

    public final static int CASH = 1;
    public final static int CREDIT_CARD = 2;
    public final static int NETS = 3;

    protected DispenseController dispenseCtrl;
    protected TransactionController trnxController;
   
    public PaymentController(TransactionController trnxController) {
        this.trnxController = trnxController;
        dispenseCtrl = new DispenseController(trnxController);
       
    }

    public abstract boolean makePayment();
    public abstract void processMoneyReceived(int total);
    public  void completeTransaction(){
         this.trnxController.refreshMachineryDisplay();
         this.trnxController.setTransactionRunning(false);
         this.trnxController.updateDrinkSelectionItemsState();
    
    }
    
    public abstract CoinReceiver getCoinReceiver() throws UnsupportedOperationException ;
    public abstract NETSCardReceiver getNETSCardReceiver() throws UnsupportedOperationException ;
    public abstract CreditCardReceiver getCreidtCardReceiver() throws UnsupportedOperationException ;

    public void displayCustomerPanel() {
        dispenseCtrl.updateDrinkPanel();
    }

    public CustomerPanel getCustomerPanel() {
        return trnxController.getCustomerPanel();
    }

    public void terminateFault(){		

	}
    
    public void terminateTransaction(){
		
	}
    
    public void cancelTransaction(){
	}
    
    public void refreshCustomerPanel(){
                dispenseCtrl.updateDrinkPanel();
	}

    public TransactionController getTrnxController() {
        return trnxController;
    }

    public void setTrnxController(TransactionController trnxController) {
        this.trnxController = trnxController;
    }

   public DispenseController getDispenseCtrl() {
           return dispenseCtrl;
   }

   public void setDispenseCtrl(DispenseController dispenseCtrl) {
           this.dispenseCtrl = dispenseCtrl;
   }
    
    
}
