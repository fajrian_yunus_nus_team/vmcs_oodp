/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.vmcs.store;

/**
 *
 * @author sawthandaoo
 */
public abstract class StoreItemIterator {

    private final Store store;
    private int current;

    public StoreItemIterator(Store store) {
        current = 0;
        this.store = store;
    }

    public boolean hasNext() {
        if (this.store.getItems() == null || this.store.getStoreSize() <= 0) {
            return false;
        } else if (current >= this.store.getStoreSize()) {
            return false;
        }
        return true;
    }

    public StoreItem next() {
        int index = current;
        current++;
        return this.store.getStoreItem(index);
    }

    public StoreItem first() {
        current = 0;
        return this.store.getStoreItem(current);
    }

    public int getCurrent() {
        return this.current;
    }

}
